import { Router } from "express";

export const router = Router();

router.get("/sum/:a/:b", (req, res) => {
  const params = req.params;
  res.status(200).send({ msg: `Sum: ${Number(params.a) + Number(params.b)}` });
});
