import request from "supertest";
import app from "../../server";

describe("Post Endpoints", () => {
  it("should get no sum because params are missing", async () => {
    const res = await request(app).get("/sum");
    expect(res.status).toEqual(404);
  });

  it("should get 36", async () => {
    const res = await request(app).get("/sum/22/14");
    expect(res.status).toEqual(200);
    expect(res.body).toHaveProperty("msg");
    expect(res.body.msg).toBe("Sum: 36");
  });

  it("should get 10", async () => {
    const res = await request(app).get("/sum/-22/32");
    expect(res.status).toEqual(200);
    expect(res.body).toHaveProperty("msg");
    expect(res.body.msg).toBe("Sum: 10");
  });

  it("should get NaN", async () => {
    const res = await request(app).get("/sum/a/32");
    expect(res.status).toEqual(200);
    expect(res.body).toHaveProperty("msg");
    expect(res.body.msg).toBe("Sum: NaN");
  });
});
